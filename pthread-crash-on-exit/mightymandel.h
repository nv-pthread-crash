// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef MIGHTYMANDEL_H
#define MIGHTYMANDEL_H 1

#include <stdbool.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

struct mightymandel_s;
typedef struct mightymandel_s mightymandel_t;

extern mightymandel_t *mightymandel_new(GLFWwindow *parent);
extern void mightymandel_delete(mightymandel_t *context);

extern void mightymandel_start(mightymandel_t *context, GLuint texture);
extern void mightymandel_stop(mightymandel_t *context);
extern void mightymandel_wait(mightymandel_t *context);
extern bool mightymandel_wait_timeout(mightymandel_t *context, double timeout);
extern void mightymandel_refresh(mightymandel_t *context);

#define WIDTH 320
#define HEIGHT 200

#endif
