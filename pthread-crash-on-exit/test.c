// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "mightymandel.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char *simple_vert =
  "#version 430 core\n"
  "out vec2 texCoord;\n"
  "const vec2 t[4] = vec2[4](vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 1.0));\n"
  "void main() {\n"
  "  gl_Position = vec4(t[gl_VertexID] * 2.0 - 1.0, 0.0, 1.0);\n"
  "  texCoord = t[gl_VertexID];\n"
  "}\n"
  ;

const char *simple_frag =
  "#version 430 core\n"
  "uniform sampler2D tex;\n"
  "in vec2 texCoord;\n"
  "out vec4 colour;\n"
  "int px(vec2 tc) {\n"
  "  vec4 t = texture(tex, tc);\n"
  "  int k = 0;\n"
  "  k += (t.x / 2.0) - floor(t.x / 2.0) >= 0.5 ? 1 : 2;\n"
  "  k += t.z >= 0.0 ? 4 : 8;\n"
  "  k += 16 * int(floor(t.x));\n"
  "  return k;\n"
  "}\n"
  "void main() {\n"
  "  vec2 dx = dFdx(texCoord);\n"
  "  vec2 dy = dFdy(texCoord);\n"
  "  float c = px(texCoord) == px(texCoord + dx + dy) && px(texCoord + dx) == px(texCoord + dy) ? 1.0 : 0.0;\n"
  "  float me = texture(tex, texCoord).x;\n"
  "  colour = vec4(me <= 0.0 ? vec3(1.0, 0.0, 0.0) : vec3(c), 1.0);\n"
  "}\n"
  ;


int main() {

  glfwInit();
  int win_width = WIDTH;
  int win_height = HEIGHT;
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(win_width, win_height, "mightymandel", 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, win_width, win_height, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &simple_vert, 0);
    glCompileShader(shader);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &simple_frag, 0);
    glCompileShader(shader);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  {
    char buf[10000];
    int len = 0;
    glGetProgramInfoLog(program, 10000, &len, buf);
    if (len > 0) {
      buf[len] = 0;
      fprintf(stderr, "%s\n", buf);
    }
  }
  glUseProgram(program);

  mightymandel_t *context = mightymandel_new(window);
  assert(context);
  for (int pass = 0; pass < 3; ++pass) {
    mightymandel_start(context, tex);

    for (int f = 0; f < 10; ++f) {
    if (! mightymandel_wait_timeout(context, 1.0 / 30.0)) {
      mightymandel_refresh(context);
      glBindTexture(GL_TEXTURE_2D, tex);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      glfwSwapBuffers(window);
    }
      glfwPollEvents();
      if (glfwWindowShouldClose(window)) {
        break;
      }
    }
    if (glfwWindowShouldClose(window)) {
      break;
    }
  }
  mightymandel_delete(context);

  glfwDestroyWindow(window);
  glfwTerminate();

  return 0;
}
