// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// context thread

#include "mightymandel_private.h"
#include <assert.h>
#include <stdio.h>

#define CHECK(e) do{ \
  int pthreading_error = (e); \
  assert(! pthreading_error); \
}while(0)

void *_mightymandel_main(void *arg) {
  fprintf(stderr, "thread begin\n"); // helps provoke race condition
  mightymandel_t *context = arg;
  assert(context);
  assert(context->window);
  CHECK(pthread_mutex_lock(&context->mutex));

  glfwMakeContextCurrent(context->window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  _mightymandel_fp32_init(context);

  CHECK(pthread_cond_signal(&context->cond));
  bool running = true;
  while (running) {
    CHECK(pthread_cond_wait(&context->cond, &context->mutex));
    if (context->quit_request) {
      context->quit_request = false;
      running = false;
    } else if (context->start_request) {
      context->start_request = false;
      context->texture = context->start_texture;
      context->start_texture = 0;
      context->state = mightymandel_state_active;
      CHECK(pthread_cond_signal(&context->cond));
      CHECK(pthread_mutex_unlock(&context->mutex));

      _mightymandel_fp32_render(context, context->texture);

      CHECK(pthread_mutex_lock(&context->mutex));
      if (context->stop_request) {
        context->stop_request = false;
        CHECK(pthread_cond_signal(&context->cond));
      }
      context->state = mightymandel_state_idle;
    }
  }
  glfwMakeContextCurrent(0);
  CHECK(pthread_mutex_unlock(&context->mutex));
  fprintf(stderr, "thread end\n"); // helps provoke race condition
  return 0;
}

bool _mightymandel_refresh(mightymandel_t *context, GLuint buffer) {
  assert(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  bool retval;
  if (context->quit_request || context->stop_request) {
    retval = true;
  } else if (context->refresh_request) {
    retval = false;
    context->refresh_request = false;
    assert(buffer);
    assert(context->texture);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
    glBindTexture(GL_TEXTURE_2D, context->texture);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, WIDTH, HEIGHT, GL_RGBA, GL_FLOAT, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    GLsync sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    context->refresh_sync = sync;
    CHECK(pthread_cond_signal(&context->cond));
  }
  CHECK(pthread_mutex_unlock(&context->mutex));
  return retval;
}
