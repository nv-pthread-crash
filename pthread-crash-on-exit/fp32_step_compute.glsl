// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(location = 1) uniform uint step_iterations;
layout(location = 2) uniform uvec3 size;
layout(location = 3) uniform vec4 view;
layout(location = 5) uniform uint start_iterations;

layout(std430, binding = 1) buffer b_context {
  readonly  restrict uint active_in;
  writeonly restrict uint escaped;
};

layout(std430, binding = 2) buffer b_results {
  writeonly restrict vec4 results[];
};

layout(std430, binding = 3) buffer b_state {
          restrict uvec4 state[];
};

layout(local_size_x = 1024) in;

void main() {
  if (gl_GlobalInvocationID.x < active_in) {
    uvec4 me = state[gl_GlobalInvocationID.x];
    if (me.z < size.z) {
      uint i = me.z % size.x;
      uint j = me.z / size.x;
      float x = (float(i) + 0.5) / float(size.x) - 0.5;
      float y = (float(j) + 0.5) / float(size.y) - 0.5;
      vec2 c = vec2(x, y) * 2.0 * vec2(float(size.x) / float(size.y), 1.0) * view.z + view.xy;
      float zx = uintBitsToFloat(me.x);
      float zy = uintBitsToFloat(me.y);
      float zx2 = zx * zx;
      float zy2 = zy * zy;
      float mz2 = zx2 + zy2;
      uint m = 0;
      for (uint k = 0; k < step_iterations; ++k) {
        if (! (mz2 < view.w)) { break; }
        float nzx = c.x + zx2 - zy2;
        float nzy = c.y + 2.0 * zx * zy;
        zx = nzx;
        zy = nzy;
        zx2 = zx * zx;
        zy2 = zy * zy;
        mz2 = zx2 + zy2;
        m = k + 1;
      }
      me.x = floatBitsToUint(zx);
      me.y = floatBitsToUint(zy);
      if (! (mz2 < view.w)) {
        results[me.z] = vec4(float(start_iterations + m), zx, zy, 0.0);
        me.z = size.z;
        escaped = 1;
      }
      state[gl_GlobalInvocationID.x] = me;
    }
  }
}
