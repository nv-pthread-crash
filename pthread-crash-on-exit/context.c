// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// public context API, called from main thread

#include "mightymandel_private.h"

#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#undef CHECK
#define CHECK(e) do{ \
  int pthreading_error = (e); \
  assert(! pthreading_error); \
}while(0)

extern mightymandel_t *mightymandel_new(GLFWwindow *parent) {
  assert(parent);
  glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(16, 16, "hidden", 0, parent);
  assert(window);

  mightymandel_t *context = malloc(sizeof(*context));
  assert(context);
  memset(context, 0, sizeof(*context));

  context->window = window;

  CHECK(pthread_mutex_init(&context->mutex, 0));
  CHECK(pthread_cond_init(&context->cond, 0));

  CHECK(pthread_mutex_lock(&context->mutex));
  CHECK(pthread_create(&context->thread, 0, _mightymandel_main, context));
  CHECK(pthread_cond_wait(&context->cond, &context->mutex));
  CHECK(pthread_mutex_unlock(&context->mutex));

  return context;
}

extern void mightymandel_delete(mightymandel_t *context) {
  assert(context);
  mightymandel_stop(context);
  mightymandel_wait(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  assert(context->state == mightymandel_state_idle);
  context->quit_request = true;
  CHECK(pthread_cond_signal(&context->cond));
  CHECK(pthread_mutex_unlock(&context->mutex));
  CHECK(pthread_join(context->thread, 0));
  CHECK(pthread_mutex_destroy(&context->mutex));
  CHECK(pthread_cond_destroy(&context->cond));
  glfwDestroyWindow(context->window);
  free(context);
}

extern void mightymandel_start(mightymandel_t *context, GLuint texture) {
  assert(context);
  assert(texture);
  mightymandel_stop(context);
  mightymandel_wait(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  assert(context->state == mightymandel_state_idle);
  context->start_request = true;
  context->start_texture = texture;
  CHECK(pthread_cond_signal(&context->cond));
  CHECK(pthread_cond_wait(&context->cond, &context->mutex));
  CHECK(pthread_mutex_unlock(&context->mutex));
}

extern void mightymandel_stop(mightymandel_t *context) {
  assert(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  if (context->state == mightymandel_state_active) {
    context->stop_request = true;
    CHECK(pthread_cond_signal(&context->cond));
    CHECK(pthread_cond_wait(&context->cond, &context->mutex));
  }
  CHECK(pthread_mutex_unlock(&context->mutex));
}

extern void mightymandel_refresh(mightymandel_t *context) {
  assert(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  mightymandel_state_t s = context->state;
  context->refresh_request = true;
  if (s == mightymandel_state_active) {
    CHECK(pthread_cond_wait(&context->cond, &context->mutex));
    GLsync sync = context->refresh_sync;
    context->refresh_sync = 0;
    CHECK(pthread_mutex_unlock(&context->mutex));
    glWaitSync(sync, 0, GL_TIMEOUT_IGNORED);
  } else {
    CHECK(pthread_mutex_unlock(&context->mutex));
  }
}

extern void mightymandel_wait(mightymandel_t *context) {
  assert(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  mightymandel_state_t s = context->state;
  if (s == mightymandel_state_active) {
    CHECK(pthread_cond_wait(&context->cond, &context->mutex));
  }
  CHECK(pthread_mutex_unlock(&context->mutex));
}

extern bool mightymandel_wait_timeout(mightymandel_t *context, double timeout) {
  assert(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  mightymandel_state_t s = context->state;
  bool retval;
  if (s == mightymandel_state_idle) {
    retval = true;
  } else {
    struct timespec abstime;
    clock_gettime(CLOCK_REALTIME, &abstime);
    time_t s = floor(timeout);
    long ns = floor(1.0e9 * (timeout - s));
    abstime.tv_sec += s;
    abstime.tv_nsec += ns;
    if (abstime.tv_nsec >= 1000000000) {
      abstime.tv_nsec -= 1000000000;
      abstime.tv_sec += 1;
    }
    retval = ! pthread_cond_timedwait(&context->cond, &context->mutex, &abstime);
  }
  CHECK(pthread_mutex_unlock(&context->mutex));
  return retval;
}
