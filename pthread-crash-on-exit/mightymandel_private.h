// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef MIGHTYMANDEL_PRIVATE_H
#define MIGHTYMANDEL_PRIVATE_H 1

#include "mightymandel.h"

#include <stdbool.h>
#include <pthread.h>

enum mightymandel_state_e
  { mightymandel_state_idle = 0
  , mightymandel_state_active
  };
typedef enum mightymandel_state_e mightymandel_state_t;

struct fp32_s;
typedef struct fp32_s fp32_t;

struct mightymandel_s {
  // library thread arguments
  GLFWwindow *parent;

  // library thread
  pthread_t thread;

  // big global lock
  pthread_mutex_t mutex;
  pthread_cond_t cond;

  // shared state (only access when locked)
  mightymandel_state_t state;
  bool quit_request;
  bool start_request;
  GLuint start_texture;
  bool stop_request;
  bool refresh_request;
  GLsync refresh_sync;

  // GL state
  GLFWwindow *window;

  // options from last render
  GLuint texture;

  // renderers
  fp32_t *fp32;
};

void *_mightymandel_main(void *arg);

bool _mightymandel_refresh(mightymandel_t *context, GLuint buffer);

GLuint _mightymandel_compute_shader(const char *label, const char *compute_src);
void _mightymandel_fp32_init(mightymandel_t *context);
void _mightymandel_fp32_render(mightymandel_t *context, GLuint texture);

#endif
