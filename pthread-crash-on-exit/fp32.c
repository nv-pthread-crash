// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "mightymandel_private.h"

#include "fp32_init_compute.glsl.c"
#include "fp32_step_compute.glsl.c"

#include <assert.h>
#include <stdlib.h>

struct fp32_s {
  GLuint p_init, p_step;
  GLuint b_context, b_results, b_state1, b_workgroup;
};

GLuint _mightymandel_compute_shader(const char *label, const char *comp) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(shader, 1, &comp, 0);
    glCompileShader(shader);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  glObjectLabel(GL_PROGRAM, program, -1, label);
  return program;
}

void _mightymandel_fp32_init(mightymandel_t *context) {
  assert(context);
  fp32_t *fp32 = malloc(sizeof(*fp32));
  assert(fp32);

  fp32->p_init = _mightymandel_compute_shader("fp32_init", _mightymandel_fp32_init_compute);
  fp32->p_step = _mightymandel_compute_shader("fp32_step", _mightymandel_fp32_step_compute);

  context->fp32 = fp32;
}

void _mightymandel_fp32_render(mightymandel_t *context, GLuint texture) {
  assert(context);
  assert(texture);
  fp32_t *fp32 = context->fp32;
  assert(fp32);

  // unpack options
  GLuint step_iterations = 64;
  GLuint width = WIDTH;
  GLuint height = HEIGHT;
  GLuint size = width * height;

  GLfloat center_re = -0.75;
  GLfloat center_im = 0.0;
  GLfloat radius = 1.5;
  GLfloat escape_radius_2 = 25 * 25;

  // allocate buffers

  glGenBuffers(1, &fp32->b_context);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_context);
  glObjectLabel(GL_BUFFER, fp32->b_context, -1, "fp32_context");

  glGenBuffers(1, &fp32->b_results);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_results);
  glObjectLabel(GL_BUFFER, fp32->b_results, -1, "fp32_results");

  glGenBuffers(1, &fp32->b_state1);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_state1);
  glObjectLabel(GL_BUFFER, fp32->b_state1, -1, "fp32_state[0]");

  glGenBuffers(1, &fp32->b_workgroup);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_workgroup);
  glObjectLabel(GL_BUFFER, fp32->b_workgroup, -1, "fp32_workgroup");

  // initialize buffers

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_context);
  GLuint b_context_data[2] = { size, 0 };
  glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(GLuint), &b_context_data[0], GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, fp32->b_context);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_results);
  glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(GLfloat), 0, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, fp32->b_results);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_state1);
  glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(GLfloat), 0, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, fp32->b_state1);

  GLuint workgroups = (size + 1023) / 1024;
  workgroups = workgroups == 0 ? 1 : workgroups;
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_workgroup);
  GLuint b_workgroup_data[3] = { workgroups, 1, 1 };
  glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(GLuint), b_workgroup_data, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, fp32->b_workgroup);

  glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, fp32->b_workgroup);

  // initialize uniforms

  glUseProgram(fp32->p_init);
  glUniform3ui(2, width, height, size);
  glUniform4f(3, center_re, center_im, radius, escape_radius_2);

  glUseProgram(fp32->p_step);
  glUniform1ui(1, step_iterations);
  glUniform3ui(2, width, height, size);
  glUniform4f(3, center_re, center_im, radius, escape_radius_2);

  // render

  glUseProgram(fp32->p_init);
  glDispatchComputeIndirect(0);
  glMemoryBarrier(GL_ALL_BARRIER_BITS);

  for (GLuint start_iterations = 0; true; start_iterations += step_iterations) {

    if (_mightymandel_refresh(context, fp32->b_results)) { break; }

    glUseProgram(fp32->p_step);
    glUniform1ui(5, start_iterations);
    glDispatchComputeIndirect(0);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);

  }
  _mightymandel_refresh(context, fp32->b_results);

  // reset state

  for (int i = 1; i < 7; ++i) {
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, i, 0);
  }
  glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glUseProgram(0);

  // deallocate buffers
  glDeleteBuffers(1, &fp32->b_context);
  fp32->b_context = 0;

  glDeleteBuffers(1, &fp32->b_results);
  fp32->b_results = 0;

  glDeleteBuffers(1, &fp32->b_state1);
  fp32->b_state1 = 0;

  glDeleteBuffers(1, &fp32->b_workgroup);
  fp32->b_workgroup = 0;
}
