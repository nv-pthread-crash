#!/bin/bash

cat <<'EOF'

# how to build in a clean environment
# ===================================

# on host as root in a scratch directory with 1GB free space
mkdir chroot-wheezy
debootstrap wheezy chroot-wheezy http://ftp.uk.debian.org/debian
mount proc chroot-wheezy/proc -t proc
mount sysfs chroot-wheezy/sys -t sysfs
chroot chroot-wheezy /bin/bash

# in chroot as root
cd
adduser --disabled-login --add_extra_groups claude
nano /etc/apt/sources.list
# add: deb-src http://ftp.uk.debian.org/debian wheezy main
apt-get update
apt-get build-dep glfw
apt-get build-dep glew
apt-get install git xorg-dev cmake unzip
su - claude

# in chroot as claude
git clone http://code.mathr.co.uk/nv-pthread-crash.git
cd nv-pthread-crash
./build.sh BUILD
exit

# in chroot as root
exit

# on host as root
su - claude

# on host as regular user


# how to reproduce the bug
# ========================

# outside chroot
ldd chroot-wheezy/home/claude/nv-pthread-crash/build/bin/test

valgrind --tool=helgrind ./chroot-wheezy/home/claude/nv-pthread-crash/build/bin/test
# reports EINVAL from pthread_mutex_lock in libGL.so.346.72

./chroot-wheezy/home/claude/nv-pthread-crash/build/bin/test
# crashes on exit (pegs cpu in rt kernel migration threads)
# ssh in from outside to kill -9 and restore control

EOF

if [ "x${1}" = "xBUILD" ]
then

mkdir -p build/bin
BUILD="`pwd`/build"


unzip glfw*.zip
mkdir -p build-glfw
cd build-glfw
GLFW_OPTS="-DBUILD_SHARED_LIBS=0 -DCMAKE_INSTALL_PREFIX='${BUILD}' -DGLFW_BUILD_EXAMPLES=0 -DGLFW_BUILD_TESTS=0 -DGLFW_BUILD_DOCS=0 -DGLFW_CLIENT_LIBRARY=opengl"
cmake ${GLFW_OPTS} ../glfw*/
make
make install
cd ..


tar xzvf glew*.tgz
cd glew*/
make GLEW_PREFIX="${BUILD}" GLEW_DEST="${BUILD}" install
cd ..


cd pthread-crash-on-exit
make FLAGS="-I${BUILD}/include `PKG_CONFIG_PATH=${BUILD}/lib/pkgconfig pkg-config --static --libs glfw3` ${BUILD}/lib64/libGLEW.a"
cp -avit "${BUILD}/bin" test
cd ..

else

echo "not building without a BUILD argument"

fi
